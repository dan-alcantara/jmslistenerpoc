package com.poc.queue;

import java.util.Hashtable;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;

public class Listener implements MessageListener {

    private final static String JNDI_FACTORY="weblogic.jndi.WLInitialContextFactory";
    private final static String JMS_FACTORY="jms/ExtracaoExecucaoIntegracaoFactory";
    private final static String QUEUE_NAME="jms/ExtracaoExecucaoIntegracaoQueue";
    //private final static String IP = "t3://10.122.50.36:8901";
    private final static String IP = "t3://localhost:7001";
    private final static Boolean quit = false;

    public static void main(String[] args) throws Exception {

        String ip = IP;

        if (args != null && args.length > 0) {
            ip = args[0];
        }

        Listener listener = new Listener();
        listener.init(ip);
        System.out.println("Ready To Receive Messages");

        synchronized(listener)
        {
            while (! listener.quit)
            {
                try
                {
                    listener.wait();
                }
                catch (InterruptedException ie) {}
            }
        }
    }

    public void init(String ip) throws Exception {
        QueueConnectionFactory qconFactory;
        QueueConnection qcon;
        QueueSession qsession;
        QueueReceiver qreceiver;
        Queue queue;
        TextMessage message;


        Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY, JNDI_FACTORY);
        env.put(Context.PROVIDER_URL, ip);
        Context context = new InitialContext(env);

        qconFactory = (QueueConnectionFactory) context.lookup(JMS_FACTORY);
        qcon = qconFactory.createQueueConnection();
        qsession = qcon.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
        queue = (javax.jms.Queue) context.lookup(QUEUE_NAME);
        qreceiver = qsession.createReceiver(queue);
        qreceiver.setMessageListener(this);
        qcon.start();
    }

    @Override
    public void onMessage(Message msg)
    {
        try {
            String msgText;
            if (msg instanceof TextMessage)
            {
                msgText = ((TextMessage)msg).getText();
            }
            else
            {
                msgText = msg.toString();
            }

            System.out.println(msgText);
        }
        catch (JMSException jmse)
        {
            jmse.printStackTrace();
        }
    }
}
